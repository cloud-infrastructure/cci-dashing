FROM frvi/dashing
MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

COPY assets/* /assets/
COPY dashboards/* /dashboards/
COPY jobs/* /jobs/
COPY widgets/* /widgets/

