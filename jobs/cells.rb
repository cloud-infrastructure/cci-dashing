
SCHEDULER.every '600s' do
  cells = []
  (0..42).each do |num|
	v = Random.rand(2000)
	cells.push("cell#{num}")
	send_event("cell#{num}", {value: v, min: 0, max: v+Random.rand(10..3000)})

	set :cells, cells
  end

end

