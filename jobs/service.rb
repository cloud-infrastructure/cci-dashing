points = []
(1..10).each do |i|
  points << { x: i, y: rand(50) }
end
last_x = points.last[:x]

SCHEDULER.every '600s' do
  services = ['keystone','glance','cinder','neutron','nova','horizon','heat','magnum','manila','barbican']
  services.each do |service|
        points.shift
  	last_x += 1
  	points << { x: last_x, y: rand(50) }
  	send_event(service, points: points)
  end
end
