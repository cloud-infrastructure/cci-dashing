require 'influxdb'
 
dashing_config = '/etc/cci-dashing/config.yaml'
config = YAML.load_file(dashing_config)
 
influxdb = InfluxDB::Client.new host: config['influxdb']['host'], port: config['influxdb']['port'], username: config['influxdb']['user'], password: config['influxdb']['password'], use_ssl: true, verify_ssl: false, database: config['influxdb']['database']

SCHEDULER.every '10s' do

  influxdb.query 'select last(value) from cluster_num, hv_total, ports_num, project_num, user_num' do |name, tags, values|
    send_event(name, { current: values[0]['last'] })
  end

  vm_total = 0
  cell_count = 0
  influxdb.query 'select last(value), cell_name from vm_total group by cell_name' do |name, tags, values|
    vm_total += values[0]['last']
    cell_count += 1
  end
  send_event('vm_total', { current: vm_total })
  send_event('cells', { current: cell_count })

  volumes_size = 0
  influxdb.query 'select last(value) from cp1_size, cp2_size, io1_size, standard_size, "wig-cp1_size", "wig-cpio1_size"' do |name, tags, values|
    volumes_size += values[0]['last']
  end
  send_event('volumes_size', { current: volumes_size * 1024 * 1024 })

  volumes_count = 0
  influxdb.query 'select last(value) from cp1_count, cp2_count, io1_count, standard_count, "wig-cp1_count", "wig-cpio1_count"' do |name, tags, values|
    volumes_count += values[0]['last']
  end
  send_event('volumes_count', { current: volumes_count })

#   send_event('cells', { current:  })
#   send_event('shares', { current:  })
end

